#!/usr/bin/env python3
import busio
import threading
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import time 
import digitalio
import RPi.GPIO as GPIO
import os

rate = 0                         # current sampling rate position
start_time = 0                   # starting time of program
btn = 16                         # button pin (BCM)
samplingRate = {0:1, 1:5, 2:10}  # sampling rates
ldr_chan = None                  # ldr channel
temp_chan = None                 # temp sensor channel

# Labels
runtime = 'Runtime'
read = "Reading"
temp = "Temp"
ldr_reading = "LDR Reading"
ldr_voltage = "LDR Voltage"

def setup():

    global ldr_chan
    global temp_chan
    
    # create the spi bus
    spi = busio.SPI( clock = board.SCK, MISO = board.MISO, MOSI = board.MOSI )

    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)

    # create the mcp object
    mcp = MCP.MCP3008( spi, cs )

    # create an analog input channel on pin 0
    ldr_chan = AnalogIn( mcp, MCP.P2)
    temp_chan = AnalogIn( mcp, MCP.P1)

    # setup button
    GPIO.setup(btn, GPIO.IN)
    GPIO.setup(btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(btn, GPIO.FALLING, callback=btn_pressed, bouncetime=200)


def sensor_thread():
    # create the thread to run after a delay from the sampling rate
    thread = threading.Timer(samplingRate[rate], sensor_thread)
    thread.daemon = True
    thread.start()

    # update runtime
    current_time = int( time.time() - start_time )

    # Displaying ldr and temp readings
    str_temp = str( round( (temp_chan.voltage - 0.5)/0.01, 2 ) ) + " C"
    str_tempVal = temp_chan.value
    str_runtime = str(current_time) + "s"
    str_ldrValue = ldr_chan.value
    str_ldrVoltage = str( round( ldr_chan.voltage, 2 )) + " V"

    print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( str_runtime, str_tempVal, str_temp, str_ldrValue, str_ldrVoltage))


def btn_pressed(channel):
    # Update sampliing rate
    global rate

    rate = (rate + 1) % 3
    
    print("Sampling at : " + str(samplingRate[rate])+"s")
    print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( runtime, read, temp, ldr_reading, ldr_voltage))






if __name__ == "__main__":
    try:
        setup()

        # program start time.
        start_time = time.time()
        print("Sampling at : " + str(samplingRate[rate])+"s")
        print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( runtime, read, temp, ldr_reading, ldr_voltage))

        sensor_thread()
        while True:
            pass
    except KeyboardInterrupt:
        print("\nExiting program.....")
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()


